import { useRouter } from 'next/dist/client/router';
import dynamic from 'next/dynamic';
import useSWR from 'swr';
import aniApis from '../../utils/aniApiCollection';
import { Anime, BaseSingleResponse } from '../../utils/types';
import { aniApiFetcher } from '../../utils/util';

const SingleAnime = dynamic(() => import('../../components/SingleAnime'));
const Nav = dynamic(() => import('../../components/Nav'));

const AnimePage = () => {
  const router = useRouter();
  const { id } = router.query;
  const { data, error } = useSWR<BaseSingleResponse<Anime>>(
    id ? aniApis.getAnime(Array.isArray(id) ? id[0] : id) : null,
    aniApiFetcher,
  );
  if (error) return <div>failedToLoad</div>;
  if (!data) return <div>loading...</div>;

  return (
    <>
      <Nav showCloseButton />
      <>{data.data ? <SingleAnime anime={data.data} /> : null}</>
    </>
  );
};

export default AnimePage;
