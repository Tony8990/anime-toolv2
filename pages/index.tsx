import dynamic from 'next/dynamic';
import 'swiper/css/bundle';
import api from '../utils/aniApiCollection';

const Banner = dynamic(() => import('../components/Banner'));
const Nav = dynamic(() => import('../components/Nav'));
const Row = dynamic(() => import('../components/Row'));

const Home = () => (
  <div>
    <Nav />
    <Banner />
    <Row
      title='RANDOM'
      fetchUrl={api.getRandomAnime(20)}
      isSingleResponse
      isLargeRow
    />
    <Row title='ACTION' fetchUrl={api.searchAnimes({ genres: 'Action' })} />
    <Row title='FANTASY' fetchUrl={api.searchAnimes({ genres: 'Fantasy' })} />
    <Row
      title='ADVENTURE'
      fetchUrl={api.searchAnimes({ genres: 'Adventure' })}
    />
    <Row title='SHOUNEN' fetchUrl={api.searchAnimes({ genres: 'Shounen' })} />
    <Row title='MECHA' fetchUrl={api.searchAnimes({ genres: 'Mecha' })} />
    <Row
      title='SUPER POWER'
      fetchUrl={api.searchAnimes({ genres: 'Super%20Power' })}
    />
    <Row title='SEINEN' fetchUrl={api.searchAnimes({ genres: 'Seinen' })} />
    <Row title='ANIMALS' fetchUrl={api.searchAnimes({ genres: 'Animals' })} />
    <Row title='DRAMA' fetchUrl={api.searchAnimes({ genres: 'Drama' })} />
    <Row
      title='TRAGEDY'
      fetchUrl={api.searchAnimes({ genres: 'Male%20Protagonist' })}
    />
  </div>
);

export default Home;
