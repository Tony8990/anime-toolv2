const withPlugins = require('next-compose-plugins');
const withImages = require('next-images');

module.exports = withPlugins([withImages], {
  images: {
    domains: ['s4.anilist.co'],
  },
  swcMinify: true,
});
