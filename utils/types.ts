export const aniApiSource = 'https://api.aniapi.com';
export enum admittedLanguages {
  en = 'en',
  it = 'it',
}
export interface BaseApiResponse {
  status_code: number;
  message: string;
  version: string;
}
export type BasePaginatedResponse<T> = BaseApiResponse & {
  data: PaginatedData<T>;
};

export type BaseSingleResponse<T> = BaseApiResponse & {
  data: T;
};

export interface PaginatedData<T> {
  current_page: number;
  last_page: number;
  count: number;
  documents: T[];
}

export type Anime = {
  id: number;
  anilist_id: number;
  mal_id?: number;
  format: Format;
  status: Status;
  titles: { [key: string]: string };
  descriptions: { [key: string]: string };
  start_date?: Date;
  end_date?: Date;
  season_period: SeasonPeriod;
  season_year?: number;
  episodes_count: number;
  episode_duration?: number;
  trailer_url?: string;
  cover_image: string;
  cover_color: string;
  banner_image: string;
  genres: { [key: string]: string };
  sequel?: number;
  prequel?: number;
  score: number;
};

export enum Format {
  TV = 0,
  TV_SHORT = 1,
  MOVIE = 2,
  SPECIAL = 3,
  OVA = 4,
  ONA = 5,
  MUSIC = 6,
}
export enum Status {
  FINISHED = 0,
  RELEASING = 1,
  NOT_YET_RELEASED = 2,
  CANCELLED = 3,
}

export enum SeasonPeriod {
  WINTER = 0,
  SPRING = 1,
  SUMMER = 2,
  FALL = 3,
  UNKNOWN = 4,
}

export type Episode = {
  id: number;
  anime_id: number;
  number: number;
  title: string;
  video: string;
  source: string;
  locale: string;
};
