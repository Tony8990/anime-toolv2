import { aniApiSource } from './types';

export const replaceYTUri = (uri?: string) =>
  uri?.replace('https://www.youtube.com/embed/', '') ?? '';

export const truncate = (str: string, n: number) =>
  str?.length > n ? str.substr(0, n - 1) + '...' : str;

export const aniApiFetcher = <T>(url: string) =>
  fetch(aniApiSource + url).then((res) => res.json() as Promise<T>);

export const revalidationOptions = {
  revalidateOnFocus: false,
  revalidateOnReconnect: false,
};
