export type SearchFilter = {
  pageLimit?: number;
  page?: number;
  genres?: string;
  title?: string;
  year?: number;
};

const aniApis = {
  getAnime: (id: string) => `/v1/anime/${id}`,
  getRandomAnime: (count = 1) => `/v1/random/anime/${count}`,
  getEpisodesList: (
    animeId: number,
    page: number,
    source?: string,
    locale?: string,
  ) => `/v1/episode?anime_id=${animeId}&per_page=20&page=${page}${
    source ? '&source=' : ''
  }${source || ''}&locale=${locale || 'it'}
  `,
  searchAnimes: (filter: SearchFilter) =>
    `/v1/anime?nsfw=true&per_page=${filter.pageLimit ?? 15}${
      filter.page ? '&page=' : ''
    }${filter.page ? filter.page : ''}${filter.genres ? '&genres=' : ''}${
      filter.genres || ''
    }${filter.title ? '&title=' : ''}${filter.title || ''}${
      filter.year ? '&year=' : ''
    }${filter.year || ''}`,
};

export type AniApiK = keyof typeof aniApis;
export type AniApiV = typeof aniApis[AniApiK];
export default aniApis;
