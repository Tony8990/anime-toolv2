import { SavedSearch } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Fab,
  Grid,
  IconButton,
  LinearProgress,
  Paper,
  TextField,
} from '@mui/material';
import { styled } from '@mui/system';
import { useDebounceCallback } from '@react-hook/debounce';
import Link from 'next/link';
import {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import aniApis from '../utils/aniApiCollection';
import { aniApiSource, Anime, BasePaginatedResponse } from '../utils/types';

const Item = styled(Paper)(({ theme }) => ({
  padding: '2px',
  textAlign: 'center',
  color: 'whitesmoke',
}));

const Search = () => {
  const [openSearch, toggleSearch] = useState(false);
  const [search, setSearch] = useState('');
  const [searchedData, updateSearchedData] = useState<Anime[]>([]);
  const handleOpen = () => toggleSearch(true);
  const handleClose = () => toggleSearch(false);
  return (
    <>
      <Fab
        variant='extended'
        size='medium'
        color='primary'
        onClick={handleOpen}
        aria-label='add'>
        <SavedSearch sx={{ mr: 1 }} />
        Search Anime
      </Fab>
      <Dialog
        fullScreen
        open={openSearch}
        onClose={handleClose}
        PaperProps={{
          className: 'bg-gray-800 bg-opacity-95 blur-md',
        }}>
        <SearchDialog
          onClose={handleClose}
          search={search}
          searchedData={searchedData}
          setSearch={setSearch}
          updateSearchedData={updateSearchedData}
        />
      </Dialog>
    </>
  );
};

const SearchDialog = ({
  search,
  searchedData,
  setSearch,
  updateSearchedData,
  onClose,
}: {
  search: string;
  searchedData: Anime[];
  setSearch: Dispatch<SetStateAction<string>>;
  updateSearchedData: Dispatch<SetStateAction<Anime[]>>;
  onClose: () => void;
}) => {
  const [whileLoading, toggleLoad] = useState(false);
  const onInputChange = useDebounceCallback(
    useCallback(async () => {
      const callSearch = await fetch(
        `${aniApiSource}${aniApis.searchAnimes({
          title: search,
          pageLimit: 8,
        })}`,
      );
      toggleLoad(false);
      if (callSearch.status !== 200) return updateSearchedData([]);
      const parseCall =
        (await callSearch.json()) as BasePaginatedResponse<Anime>;
      return updateSearchedData(parseCall.data.documents);
    }, [search, updateSearchedData]),
    500,
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  useEffect(() => {
    if (search) {
      toggleLoad(true);
      updateSearchedData([]);
      onInputChange();
    }
  }, [onInputChange, search, updateSearchedData]);

  return (
    <span>
      <DialogTitle sx={{ position: 'static', height: '15px' }}>
        {onClose ? (
          <IconButton
            aria-label='close'
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
      <DialogContent className='text-gray-50'>
        <Grid container spacing={3} marginTop={3}>
          <Grid item xs={12} className='flex justify-end content-end'>
            <TextField
              color='info'
              variant='filled'
              label='waiting for input...'
              id='fullWidth'
              className='bg-gray-100 bg-opacity-20'
              InputLabelProps={{
                className: 'text-gray-50 border-gray-50 placeholder-gray-50',
              }}
              InputProps={{
                className: 'text-gray-50 border-gray-50 placeholder-gray-50',
              }}
              value={search}
              onChange={handleChange}
            />
          </Grid>
          {searchedData?.length ? (
            searchedData.map((sm) => (
              <Grid item key={sm.id} xs={6} md={3} padding={1}>
                <Link href={`/anime/${sm.id}`} passHref>
                  <Item
                    className='flex flex-col-reverse place-content-start content-end pr-2'
                    elevation={6}
                    sx={{
                      minHeight: '248px',
                      height: '100%',
                      background: `linear-gradient(rgba(0,0,0,.6), rgba(255,255,255,.4)), url("${sm.cover_image}")`,
                      backgroundPosition: 'center',
                      backgroundSize: 'cover',
                      textAlign: 'end',
                    }}>
                    <div className='antialiased text-xl text-italic text-medium text-gray-300'>
                      {sm.titles?.en}
                    </div>
                  </Item>
                </Link>
              </Grid>
            ))
          ) : (
            <Grid item xs={12} padding={1}>
              {!whileLoading ? (
                search ? (
                  'No data for your search.'
                ) : (
                  ''
                )
              ) : (
                <LinearProgress color='info' />
              )}
            </Grid>
          )}
        </Grid>
      </DialogContent>
    </span>
  );
};

export default Search;
