import { OndemandVideo } from '@mui/icons-material';
import DownloadIcon from '@mui/icons-material/Download';
import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied';
import {
  IconButton,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@mui/material';
import { Box, SxProps } from '@mui/system';
import { useState } from 'react';
import ReactPlayer from 'react-player';
import { Episode } from '../utils/types';

const style: SxProps = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  height: 500,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const EpisodesTable = ({ episodes }: { episodes: Episode[] }) => {
  const [openEpVid, toggleEpVid] = useState(false);
  const [animeVideo, setAnimeVideo] = useState<string>('');

  const handleEpVid = (videoUrl: string) => {
    toggleEpVid(true);
    setAnimeVideo(videoUrl);
  };
  const handleCloseEp = () => {
    toggleEpVid(false);
    setAnimeVideo('');
  };
  return (
    <Table
      sx={{ minWidth: 650, width: '95%', margin: '0 auto' }}
      aria-label='simple table'
      className='text-center'>
      <TableHead>
        <TableRow>
          <TableCell align='center'>Episode</TableCell>
          <TableCell align='center'>Title</TableCell>
          <TableCell align='center'>Source</TableCell>
          <TableCell align='center'>Streaming</TableCell>
          <TableCell align='center'>Download</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {!episodes.length ? (
          <TableRow>
            <TableCell align='center'>
              <SentimentVeryDissatisfiedIcon
                style={{ fill: 'white' }}
                fontSize='large'
              />{' '}
            </TableCell>
            <TableCell align='center'>No availables episodes...</TableCell>
            <TableCell />
            <TableCell />
            <TableCell />
          </TableRow>
        ) : null}
        {episodes.map((episode) => (
          <TableRow
            key={`${episode.number}-${episode.source}`}
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell align='center' component='th' scope='row'>
              {episode?.number}
            </TableCell>
            <TableCell align='center'>{episode?.title ?? 'no-title'}</TableCell>
            <TableCell align='center'>{episode?.source}</TableCell>
            <TableCell align='center'>
              <IconButton
                size='large'
                onClick={() => handleEpVid(episode?.video)}>
                <OndemandVideo style={{ fill: 'white' }} />
              </IconButton>
            </TableCell>
            <TableCell align='center'>
              <IconButton size='large' href={episode?.video}>
                <DownloadIcon style={{ fill: 'white' }} />
              </IconButton>
            </TableCell>
          </TableRow>
        ))}
        <Modal
          open={openEpVid}
          onClose={handleCloseEp}
          aria-labelledby='modal-modal-title'
          aria-describedby='modal-modal-description'>
          <Box sx={style}>
            <ReactPlayer
              url={animeVideo}
              width='100%'
              height='100%'
              controls={true}
            />
          </Box>
        </Modal>
      </TableBody>
    </Table>
  );
};
export default EpisodesTable;
