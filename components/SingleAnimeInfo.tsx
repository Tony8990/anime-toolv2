import { CSSProperties } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Chip,
  FormControl,
  FormHelperText,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Rating,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';
import Link from 'next/link';
import { Dispatch, SetStateAction } from 'react';
import { admittedLanguages, Anime, SeasonPeriod, Status } from '../utils/types';

const listTypesStyle: CSSProperties = {
  fontSize: '14px',
  fontWeight: 'bold',
  marginLeft: '2em',
  padding: '4px',
  wordWrap: 'break-word',
};
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 30,
      width: 220,
    },
  },
};
const StyledRating = styled(Rating)({
  '& .MuiRating-iconEmpty': {
    color: '#fff',
  },
  '& .MuiRating-iconHover': {
    color: '#ff3d47',
  },
});

const SingleAnimeInfo = ({
  anime,
  availableEps,
  locale,
  changeLocale,
  filteredSource,
  setFilteredSource,
  availableSourcesOpts,
}: {
  anime: Anime;
  availableEps: number;
  locale: string;
  changeLocale: Dispatch<SetStateAction<string>>;
  filteredSource: string;
  setFilteredSource: Dispatch<SetStateAction<string>>;
  availableSourcesOpts: string[];
}) => {
  const handleSourcesChange = (event: SelectChangeEvent) => {
    const {
      target: { value, name },
    } = event;
    name === 'change-source' ? setFilteredSource(value) : changeLocale(value);
  };
  return (
    <Grid container spacing={1}>
      <Info
        anime={anime}
        availableEps={availableEps}
        isFiltered={!!filteredSource}
      />
      <Grid
        item
        className='flex justify-center md:justify-end'
        xs={12}
        sm={6}
        md={6}>
        <FormControl sx={{ width: 220 }}>
          <Select
            labelId='change-locale'
            id='change-locale'
            name='change-locale'
            value={locale}
            onChange={handleSourcesChange}
            input={<OutlinedInput label='Locales' />}
            MenuProps={MenuProps}
            SelectDisplayProps={{
              style: {
                backgroundColor: 'white',
              },
            }}>
            {Object.keys(admittedLanguages).map((ln) => (
              <MenuItem key={ln} value={ln}>
                <ListItemText primary={ln} />
              </MenuItem>
            ))}
          </Select>{' '}
          <FormHelperText sx={{ color: '#9c9c9c' }}>
            Change language
          </FormHelperText>
        </FormControl>
      </Grid>
      <Grid
        item
        xs={12}
        sm={6}
        md={6}
        className='flex justify-center md:justify-start'>
        <FormControl sx={{ width: 220 }}>
          <Select
            labelId='change-source'
            id='change-source'
            name='change-source'
            value={filteredSource}
            onChange={handleSourcesChange}
            input={<OutlinedInput label='Sources' />}
            MenuProps={MenuProps}
            SelectDisplayProps={{
              style: {
                backgroundColor: 'white',
              },
            }}>
            {' '}
            <MenuItem value=''>
              <em>None</em>
            </MenuItem>
            {availableSourcesOpts.map((src) => (
              <MenuItem key={src} value={src}>
                <ListItemText primary={src} />
              </MenuItem>
            ))}
          </Select>{' '}
          <FormHelperText sx={{ color: '#9c9c9c' }}>
            Filter by sources
          </FormHelperText>
        </FormControl>
      </Grid>
    </Grid>
  );
};

const Info = ({
  anime,
  availableEps,
  isFiltered,
}: {
  anime: Anime;
  availableEps: number;
  isFiltered: boolean;
}) => {
  const yearLabel =
    anime.status === Status.NOT_YET_RELEASED
      ? `will air in ${anime.season_year}, period ${
          SeasonPeriod[anime.season_period]
        }`
      : `started airing in ${anime.season_year}`;
  return (
    <>
      <Grid
        item
        xs={12}
        style={listTypesStyle}
        className='flex content-center justify-start'>
        <Chip
          avatar={<Avatar>{availableEps ?? 0}</Avatar>}
          label={`available episodes ${isFiltered ? 'from source' : ''}`}
          color={availableEps > 0 ? 'success' : 'error'}
          size='medium'
          sx={{ marginLeft: '0.6em', marginTop: '0.6em' }}
        />
      </Grid>
      <Grid
        item
        xs={12}
        md={4}
        style={listTypesStyle}
        className='flex content-center justify-center'>
        <Chip
          avatar={<Avatar>{anime?.episodes_count ?? 0}</Avatar>}
          label='episodes'
          color='primary'
          variant='outlined'
          sx={{ margin: '0.6em' }}
        />
        {anime?.episode_duration ? (
          <Chip
            avatar={<Avatar>{anime?.episode_duration ?? 0}</Avatar>}
            label='avg duration'
            color='secondary'
            variant='outlined'
            sx={{ margin: '0.6em' }}
          />
        ) : null}
      </Grid>
      <Grid
        item
        xs={12}
        md={7}
        style={listTypesStyle}
        className='flex content-center justify-end md:justify-end'>
        <Chip label={yearLabel} color='info' sx={{ margin: '0.6em' }} />
      </Grid>
      <Grid
        item
        xs={12}
        md={5}
        style={listTypesStyle}
        className='flex items-center justify-center md:justify-start'>
        <StyledRating
          className='content-center'
          name='read-only'
          value={anime.score / 10}
          precision={0.1}
          max={10}
          readOnly
        />
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        style={listTypesStyle}
        sx={{ margin: '0.6em' }}
        className='flex content-center justify-center md:justify-end'>
        {anime.prequel ? (
          <Button size='small' variant='contained' sx={{ margin: '0.6em' }}>
            <Link href={`/anime/${anime.prequel}`} passHref>
              Prequel
            </Link>
          </Button>
        ) : null}
        {anime.sequel ? (
          <Button size='small' variant='contained' sx={{ margin: '0.6em' }}>
            <Link href={`/anime/${anime.sequel}`} passHref>
              Sequel
            </Link>
          </Button>
        ) : null}
      </Grid>
      <Grid item xs={12} className='flex justify-center'>
        <Typography align='justify' className='w-5/6'>
          <span
            dangerouslySetInnerHTML={{
              __html:
                anime.descriptions?.en ??
                anime.descriptions?.it ??
                'No description.',
            }}
          />
        </Typography>
      </Grid>
    </>
  );
};

export default SingleAnimeInfo;
