import HighlightOff from '@mui/icons-material/HighlightOff';
import { AppBar, Button, IconButton, Toolbar, Typography } from '@mui/material';
import { Box } from '@mui/system';
import dynamic from 'next/dynamic';
import Image from 'next/image';
import Link from 'next/link';
import Elementone from '../public/images/element1.svg';
import Elementtwo from '../public/images/element2.svg';

const Search = dynamic(() => import('./Search'));

const Nav = ({ showCloseButton }: { showCloseButton?: boolean }) => (
  <Box sx={{ flexGrow: 1 }}>
    <AppBar position='fixed' style={{ backgroundColor: 'rgba(0,0, 0, 0.4)' }}>
      <Toolbar>
        <Link href='/' passHref>
          <IconButton
            size='large'
            edge='start'
            color='inherit'
            aria-label='open drawer'>
            <Image
              src={Elementone}
              width={40}
              height={40}
              className='nav-logo-1'
              alt='MG'
            />
            <Image
              src={Elementtwo}
              width={40}
              height={40}
              className='nav-logo-2'
              alt='TF'
            />
          </IconButton>
        </Link>
        <Typography
          variant='h6'
          noWrap
          component='div'
          sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}>
          Anime Tool
        </Typography>
        <Search />
        {showCloseButton ? (
          <Link href={`/`} as='/' passHref>
            <Button>
              <HighlightOff style={{ fill: 'white' }} />
            </Button>
          </Link>
        ) : null}
      </Toolbar>
    </AppBar>
  </Box>
);

export default Nav;
