import HighlightOff from '@mui/icons-material/HighlightOff';
import { Button } from '@mui/material';
import Link from 'next/link';
import { useState } from 'react';
import YouTube, { Options } from 'react-youtube';
import { Anime } from '../utils/types';
import { replaceYTUri, truncate } from '../utils/util';

const opts: Options = {
  height: '600',
  width: '100%',
  playerVars: {
    autoplay: 1,
  },
};
const BannerShared = ({
  animeItem,
  showDetailsButtons,
}: {
  animeItem: Anime;
  showDetailsButtons: boolean;
}) => {
  const [animeTrailer, setAnimeTrailer] = useState<string>('');

  const handleToggle = (url: string) => {
    if (animeTrailer) {
      setAnimeTrailer('');
    } else {
      const sanitazeUrl = replaceYTUri(url);
      setAnimeTrailer(sanitazeUrl);
    }
  };
  return (
    <header
      key={animeItem.id}
      className='banner'
      style={{
        background: `linear-gradient(rgba(0,0,0,.9), rgba(255,255,255,.6)), url("${
          animeItem.banner_image ?? animeItem.cover_image
        }")`,
        backgroundPosition: 'center center',
        height: '100%',
      }}>
      <div className='banner-contents'>
        <h1 className='banner-title'>{animeItem.titles?.en}</h1>
        {showDetailsButtons ? (
          <div className='banner-buttons'>
            <button
              disabled={!animeItem.trailer_url}
              className={`banner-button ${
                !animeItem.trailer_url ? 'banner-button-disabled' : ''
              }`}
              onClick={() =>
                animeItem.trailer_url && handleToggle(animeItem.trailer_url)
              }>
              {animeItem.trailer_url ? 'View trailer' : 'No trailer available'}
            </button>
            <Link href={`/anime/${animeItem.id}`} passHref>
              <button className='banner-button'>Details</button>
            </Link>
          </div>
        ) : null}
        <h1
          className='banner-description'
          dangerouslySetInnerHTML={{
            __html: truncate(
              animeItem.descriptions?.en ?? animeItem.descriptions?.it,
              150,
            ),
          }}
        />
        {animeTrailer && (
          <div
            style={{
              zIndex: 100,
              position: 'fixed',
              width: '60%',
              left: '50%',
              top: '55%',
              transform: 'translate(-50%, -50%)',
            }}>
            <div
              style={{
                position: 'absolute',
                width: '150%',
                height: '125%',
                zIndex: -1,
                filter: 'blur(100px)',
                backgroundColor: 'black',
                left: '-200px',
              }}
            />
            <div>
              <Button
                style={{ zIndex: 2000 }}
                onClick={() => handleToggle(animeTrailer)}>
                <HighlightOff style={{ fill: 'white' }} />
              </Button>
              <YouTube videoId={animeTrailer} opts={opts} />
            </div>
          </div>
        )}
      </div>
      <div className='banner-fadeBottom'></div>
    </header>
  );
};

export default BannerShared;
