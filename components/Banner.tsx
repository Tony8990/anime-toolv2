import dynamic from 'next/dynamic';
import useSWR from 'swr';
import aniApis from '../utils/aniApiCollection';
import { Anime, BaseSingleResponse } from '../utils/types';
import { aniApiFetcher, revalidationOptions } from '../utils/util';

const BannerShared = dynamic(() => import('./BannerShared'));

const Banner = ({}): JSX.Element => {
  const { data: { data: animeList = [] } = {} } = useSWR<
    BaseSingleResponse<Anime[]>
  >(aniApis.getRandomAnime(), aniApiFetcher, revalidationOptions);

  return (
    <div>
      {!animeList || !animeList.length ? null : (
        <BannerShared animeItem={animeList[0]} showDetailsButtons />
      )}
    </div>
  );
};

export default Banner;
