import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import PlayCircleFilledIcon from '@mui/icons-material/PlayCircleFilled';
import { Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import YouTube, { Options } from 'react-youtube';
import SwiperCore, { Navigation, Pagination } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import { Swiper, SwiperSlide } from 'swiper/react';
import useSWR from 'swr';
import {
  Anime,
  BasePaginatedResponse,
  BaseSingleResponse,
} from '../utils/types';
import {
  aniApiFetcher,
  replaceYTUri,
  revalidationOptions,
  truncate,
} from '../utils/util';

SwiperCore.use([Pagination, Navigation]);

const opts: Options = {
  height: '390',
  width: '100%',
  playerVars: {
    autoplay: 1,
  },
};

const Row = ({
  title,
  fetchUrl = '',
  isSingleResponse,
  isLargeRow = false,
  alreadyLoadedData = [],
  onImageClick = () => null,
}: {
  title: string;
  fetchUrl?: string;
  isSingleResponse?: boolean;
  isLargeRow?: boolean;
  alreadyLoadedData?: Anime[];
  onImageClick?: () => void;
}) => {
  const {
    data: { data: { documents = alreadyLoadedData } = {} } = {},
    error: errorPaged,
  } = useSWR<BasePaginatedResponse<Anime>>(
    !alreadyLoadedData.length && !isSingleResponse ? fetchUrl : null,
    aniApiFetcher,
    revalidationOptions,
  );
  const { data: { data = alreadyLoadedData } = {}, error: errorSingle } =
    useSWR<BaseSingleResponse<Anime[]>>(
      !alreadyLoadedData.length && isSingleResponse ? fetchUrl : null,
      aniApiFetcher,
      revalidationOptions,
    );
  const animes = isSingleResponse ? [...data] : [...documents];
  const [activeAnime, setActiveAnime] = useState<Anime | null>(null);

  const handleClick = (anime: Anime) => {
    setActiveAnime(!anime?.id || activeAnime?.id === anime.id ? null : anime);
  };

  if (errorPaged || errorSingle) return <div>failedToLoad</div>;

  return (
    <div className='row'>
      <h2 className='text-2xl tracking-wide'>{title}</h2>

      <div className='row-posters'>
        <Swiper
          slidesPerView={isLargeRow ? 5 : 7}
          spaceBetween={30}
          freeMode={true}
          pagination={{ clickable: true }}
          navigation={true}>
          {!(animes && animes.length)
            ? null
            : animes?.map((anime) => (
                <SwiperSlide key={anime.id}>
                  <Image
                    onClick={() => handleClick(anime)}
                    key={anime.id}
                    className={`row-poster ${isLargeRow && 'row-posterLarge'}`}
                    src={anime?.cover_image}
                    alt={anime?.titles?.en}
                    width={360}
                    height={240}
                  />
                </SwiperSlide>
              ))}
        </Swiper>
      </div>
      {activeAnime && (
        <>
          <Button onClick={() => handleClick(activeAnime)}>
            <HighlightOffIcon style={{ fill: 'white' }} />
          </Button>
          <Link href={`/anime/${activeAnime.id}`} passHref>
            <Button>
              <PlayCircleFilledIcon style={{ fill: 'white' }} />
            </Button>
          </Link>
          <div className='desc'>
            <Typography>Title : {activeAnime.titles?.en}</Typography>
            <Typography>
              Descriptions:{' '}
              <p
                dangerouslySetInnerHTML={{
                  __html: truncate(
                    activeAnime.descriptions?.en ??
                      activeAnime.descriptions?.it,
                    100,
                  ),
                }}
              />
            </Typography>
            <Typography>
              Episode Duration : {activeAnime.episode_duration}
            </Typography>
          </div>
          {activeAnime.trailer_url && (
            <YouTube
              videoId={replaceYTUri(activeAnime.trailer_url)}
              opts={opts}
            />
          )}
        </>
      )}
    </div>
  );
};

export default Row;
