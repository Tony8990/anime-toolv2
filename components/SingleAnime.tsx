import { makeStyles } from '@material-ui/core/styles';
import { Divider } from '@mui/material';
import Pagination from '@mui/material/Pagination';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import TableContainer from '@mui/material/TableContainer';
import dynamic from 'next/dynamic';
import { ChangeEvent, useEffect, useState } from 'react';
import useSWR from 'swr';
import { default as aniApis, default as api } from '../utils/aniApiCollection';
import {
  admittedLanguages,
  Anime,
  BasePaginatedResponse,
  Episode,
} from '../utils/types';
import { aniApiFetcher } from '../utils/util';

const BannerShared = dynamic(() => import('./BannerShared'));
const EpisodesTable = dynamic(() => import('./EpisodesTable'));
const Row = dynamic(() => import('./Row'));
const SingleAnimeInfo = dynamic(() => import('./SingleAnimeInfo'));

const useStyles = makeStyles(() => ({
  ul: {
    '& .MuiPaginationItem-root': {
      color: '#fff',
      backgroundColor: 'red',
    },
    '& .MuiPaginationItem-root.Mui-selected': {
      backgroundColor: 'transparent',
    },
  },
}));

const SingleAnime = ({ anime }: { anime: Anime }) => {
  const classes = useStyles();
  // pagination management
  const [currentLocale, changeLocale] = useState<string>(
    admittedLanguages[window.navigator.language as admittedLanguages] ?? 'it',
  );
  // pagination management
  const [currentPage, changeCurrentPage] = useState(1);
  const [lastPage, changeLastPage] = useState(1);
  const handlePageChange = (event: ChangeEvent<unknown>, value: number) => {
    changeCurrentPage(value);
  };
  // episodes list management
  const [episodes, setEpisodes] = useState<Episode[]>([]);
  const [availableSourcesOpts, updateSources] = useState<string[]>([]);

  const [filteredSource, setFilteredSource] = useState<string>('');

  const {
    data: {
      data: { documents, last_page, count } = {
        documents: [],
        last_page: 1,
        count: 1,
      },
    } = {},
    error,
  } = useSWR<BasePaginatedResponse<Episode>>(
    aniApis.getEpisodesList(
      anime.id,
      currentPage,
      filteredSource,
      currentLocale,
    ),
    aniApiFetcher,
  );
  useEffect(() => {
    if (!documents || !documents?.length) {
      changeLastPage(1);
      setEpisodes([]);
      return;
    }
    const sortedEpisode = documents
      .sort((a, b) =>
        b.source.toLocaleLowerCase() <= a.source.toLocaleLowerCase() ? -1 : 1,
      )
      .sort((a, b) => a.number - b.number);
    setEpisodes(sortedEpisode);
    changeLastPage(last_page);
    if (!filteredSource)
      updateSources(
        sortedEpisode
          .map((s) => s.source)
          .filter((v, i, a) => a.indexOf(v) === i),
      );
  }, [documents, filteredSource, last_page]);

  if (error) return <div>failedToLoad</div>;

  return (
    <div>
      <BannerShared animeItem={anime} showDetailsButtons={false} />
      <div className='Table__container'>
        <SingleAnimeInfo
          anime={anime}
          availableEps={count}
          locale={currentLocale}
          changeLocale={changeLocale}
          availableSourcesOpts={availableSourcesOpts}
          filteredSource={filteredSource}
          setFilteredSource={setFilteredSource}
        />
        <Divider variant='middle' />
        <TableContainer
          component={Paper}
          style={{ backgroundColor: 'black', color: 'white', padding: '1rem' }}>
          <EpisodesTable episodes={episodes} />
        </TableContainer>
      </div>
      <div className='flex justify-center content-center'>
        <Pagination
          size='large'
          variant='outlined'
          color='secondary'
          count={lastPage}
          classes={{ ul: classes.ul }}
          page={currentPage}
          onChange={handlePageChange}
        />
      </div>
      <div className='similar'>
        <Stack spacing={2}>
          <Row
            title='SIMILAR'
            fetchUrl={api.getRandomAnime(10)}
            isSingleResponse
            isLargeRow
          />
        </Stack>
      </div>
    </div>
  );
};

export default SingleAnime;
